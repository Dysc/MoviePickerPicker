import pickle
import random
import keys
import tweepy
import datetime

names = ["Jeff", "Michelle", "Turc", "Max", "Meghan", "Cheryl"]


def main():
    pullList, pushList = loadPickles()

    if (pullList == []):
        print("End of list. Reset...")
        pullList = names.copy()
        random.shuffle(pullList)
        pushList = []

    pick = pullList.pop()
    pushList.append(pick)

    dumpPickles([pullList, pushList], ["pullList.pickle", "pushList.pickle"])

    ## Post this week's pick to Twitter
    tweet = createTweet(pick)
    postToTwitter(tweet)


def createTweet(name):
    # Get human-readable date
    datestring = datetime.date.today().strftime("%B %d, %Y")

    # Create tweet with name parm
    tweet = "Movie for %s: %s" % (datestring, name)

    return tweet


def postToTwitter(tweet):
    # Pass OAuth process
    auth = tweepy.OAuthHandler(keys.consumer_key, keys.consumer_secret)
    auth.set_access_token(keys.access_token, keys.access_token_secret)

    # Create API interface object
    api = tweepy.API(auth)

    # Post the tweet parm
    try:
        print("Posting new tweet: %s" % tweet)
        api.update_status('%s' % tweet)
    except tweepy.TweepError as te:
        print(te)
        api.update_status('MoviePicker broke, someone tell Jeff. Error code: 2')


def loadPickles():
    ## Get open and closed lists from file
    try:
        pullfile = open("pullList.pickle", "rb")
        pullList = pickle.load(pullfile)
        pushfile = open("pushList.pickle", "rb")
        pushList = pickle.load(pushfile)
    except:
        print("Files not found. Starting fresh.")
        pullList = names.copy()
        pushList = []
        random.shuffle(pullList)

    return pullList, pushList


def dumpPickles(objList, filenameList):
    for obj, file in zip(objList, filenameList):
        try:
            with open(file, 'wb') as f:
                pickle.dump(obj, f)
                f.close()
        except FileNotFoundError as fne:
            print(fne)
            postToTwitter("MoviePicker broke, someone tell Jeff. Error code: 1")


if __name__ == "__main__":
    main()
